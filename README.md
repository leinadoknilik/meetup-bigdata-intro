# BEVEZETÉS A BIG DATA VILÁGÁBA - PÉCS IT MEETUP #

[http://www.meetup.com/Pecs-IT-Meetup/](http://www.meetup.com/Pecs-IT-Meetup)


### Mi található a repoban? ###

* A példa programok forráskódja
* Az előadás diák pdf formátumban

### Szükséges környezet ###

* Virtualizációs program [pl.:https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)

* Hortonworks HDP - [http://hortonworks.com/downloads/#data-platform](http://hortonworks.com/downloads/#data-platform) **vagy**
* Cloudera CDH - [http://www.cloudera.com/downloads/cdh/5-8-0.html](http://www.cloudera.com/downloads/cdh/5-8-0.html)
    * minimum 8Gb RAM a VM-hez rendelve 
    * minimum 2 processor mag a VM-hez rendelve
    * 10Gb image méret


* Git

* Maven

* JDK 1.7<= 

* A kedvenc fejlesztői környezeted :)


### Build project ###

* mvn clean install
* JAR a target mappa alatt

### HDFS ###
* hadoop fs -ls 'folder'
* hadoop fs -put 'folder/file' 'target_folder_on_hdfs'
* hadoop jar program.jar package.mainclass input_file output_folder
* hadoop fs -cat folder/part-r-000000

###Használt fájlok###

[http://stat-computing.org/dataexpo/2009/2007.csv.bz2](http://stat-computing.org/dataexpo/2009/2007.csv.bz2)

### Ajánlott irodalom ###

[Hadoop Definitive Guide](http://shop.oreilly.com/product/0636920033448.do)