package com.meetup.hadoop.mapreduce;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class FlightsOnFridayMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

    public static final Text SPECIAL_KEY = new Text("SPECIAL_KEY");
    public static final LongWritable ONE = new LongWritable(1);

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String[] split = value.toString().split(",");
        if(split.length >= 4) {
            if("5".equals(split[3].trim())) {
                context.write(SPECIAL_KEY, ONE);
            }
        }
    }
}
