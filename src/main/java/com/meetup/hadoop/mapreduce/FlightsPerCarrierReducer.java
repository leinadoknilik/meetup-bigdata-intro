package com.meetup.hadoop.mapreduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class FlightsPerCarrierReducer extends Reducer<Text, LongWritable, Text, LongWritable> {

    private List<FlightPerCarrier> results = new ArrayList<>();

    @Override
    protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
        long result = 0L;
        for(LongWritable num : values) {
            result+= num.get();
        }
        results.add(new FlightPerCarrier(key.toString(), result));
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        Collections.sort(results);
        Collections.reverse(results);
        for(FlightPerCarrier fpc : results) {
            context.write(new Text(fpc.getCarrier()), new LongWritable(fpc.getNumFlights()));
        }
    }

    private class FlightPerCarrier implements  Comparable<FlightPerCarrier> {

        private final String carrier;
        private final long numFlights;

        FlightPerCarrier(String carrier, long numFlights) {
            this.carrier = carrier;
            this.numFlights = numFlights;
        }

        public long getNumFlights() {
            return numFlights;
        }

        public String getCarrier() {
            return carrier;
        }

        @Override
        public int compareTo(FlightPerCarrier o) {
            return Long.compare(numFlights, o.getNumFlights());
        }
    }
}
