package com.meetup.hadoop.mapreduce;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Test;

import com.meetup.hadoop.mapreduce.FlightsOnFridayMapper;
import com.meetup.hadoop.mapreduce.FlightsOnFridayReducer;

import java.io.IOException;
import java.util.Arrays;

public class SimpleCountTest {

    private MapDriver<LongWritable, Text, Text, LongWritable> mapDriver = new MapDriver<>(new FlightsOnFridayMapper());
    private ReduceDriver<Text, LongWritable, Text, LongWritable> reduceDriver = new ReduceDriver<>(new FlightsOnFridayReducer());

    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(new LongWritable(0), new Text("2007,1,1,1,alma"));
        mapDriver.withInput(new LongWritable(0), new Text("2007,1,1,5,alma"));
        mapDriver.withInput(new LongWritable(0), new Text(""));
        mapDriver.withInput(new LongWritable(0), new Text("2007,1,1,3,alma"));
        mapDriver.withInput(new LongWritable(0), new Text("2007Ő1"));
        mapDriver.withInput(new LongWritable(0), new Text("2007,1,1,2,alma"));
        mapDriver.withInput(new LongWritable(0), new Text("2007,1,1,5,alma"));

        mapDriver.withOutput(FlightsOnFridayMapper.SPECIAL_KEY, FlightsOnFridayMapper.ONE);
        mapDriver.withOutput(FlightsOnFridayMapper.SPECIAL_KEY, FlightsOnFridayMapper.ONE);

        mapDriver.runTest();
    }

    @Test
    public void testReducer() throws IOException {
        reduceDriver.withInput(FlightsOnFridayMapper.SPECIAL_KEY, Arrays.asList(FlightsOnFridayMapper.ONE, FlightsOnFridayMapper.ONE));

        reduceDriver.withOutput(FlightsOnFridayReducer.RESULT, new LongWritable(2));
        reduceDriver.runTest();
    }




}