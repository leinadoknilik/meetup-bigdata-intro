package com.meetup.hadoop.mapreduce;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class FlightsOnFridayReducer extends Reducer<Text, LongWritable, Text, LongWritable> {

    public static final Text RESULT = new Text("RESULT");

    @Override
    protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
        long result = 0L;
        for(LongWritable num : values) {
            result+= num.get();
        }
        context.write(RESULT, new LongWritable(result));
    }
}
