package com.meetup.hadoop.mapreduce;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FlightsPerCarrierMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

    private Map<String, Long> carriers = new HashMap<>();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        if(!value.toString().startsWith("Year,")) {
            String[] split = value.toString().split(",");
            if (split.length >= 9) {
                String carrier = split[8];
                if (!carrier.isEmpty()) {
                    Long num = carriers.get(carrier);
                    if (num == null) {
                        num = 1L;
                    } else {
                        num++;
                    }
                    carriers.put(carrier, num);
                }
            }
        }
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        for(String carrier : carriers.keySet()) {
            context.write(new Text(carrier), new LongWritable(carriers.get(carrier)));
        }
    }
}
